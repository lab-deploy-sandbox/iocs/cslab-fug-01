# This should be a startup script
# do not use other startup scriptis e.g.from "e3-fug/fug/cmds"
# Comment or uncoment the appriopriate line with the right snippet
#

require stream,2.8.10
require fug, master

# load fug_LV.iocsh, fug-01 for RFLab test
iocshLoad("$(fug_DIR)/fug_LV.iocsh", "Ch_name=RF-01, IP_addr=172.30.5.114, PORT=2101, P=RFLab:, R=fug-01:, EGU=V, ASLO=1, fug_DB=$(fug_DIR)/db")

# load fug_HV.iocsh, fug-01 for ISrc + software interlock
# iocshLoad("$(fug_DIR)/fug_HV.iocsh", "Ch_name=ISrc-01, IP_addr=172.30.5.250, PORT=2101, P=ISrc:, R=fug-01:, EGU=kV, ASLO=0.001, fug_DB=$(fug_DIR)/db")